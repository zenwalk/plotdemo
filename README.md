该项目来源于GISpace，作者博客：http://blog.csdn.net/gispace/article/details/38961441

动态标绘演示系统1.4，基于动态标绘API 1.4。实现功能包括：

1、标绘符号绘制（PlotDrawTool）；

2、标绘符号编辑（PlotEditTool）；

3、标绘符号样式编辑（参见演示系统源码）；

4、标绘符号文本标注（1.4新增PlotGraphic，支持文本标注）；

5、标绘符号保存与打开（通过序列化接口PlotEncoder、反序列化接口PlotDecoder）；

新增标绘符号文本标注

该版本主要新增标绘符号的文本标注功能，新增PlotGraphic（继承自Graphic），同步更新序列化与反序列化功能，支持文本标注。

简单起见，示例程序中采用保存、打开json文本文件的方式。

在实际项目中，可根据具体情况选择存储方式，比如数据库。

在线体验地址：http://gispace.duapp.com/plotdemo/index.html

源码下载 点击下载
![20140831172808678.png](https://bitbucket.org/repo/arBdzM/images/3495328328-20140831172808678.png)

